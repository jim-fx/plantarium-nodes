import { throttle } from 'helpers';
export default class EventEmitter {
  cbs: { [key: string]: ((data: any) => void)[] } = {};

  emit(event: string, data?: any) {
    if (event in this.cbs) {
      this.cbs[event].forEach((c) => c(data));
    }
  }

  on(event: string, cb: (data: any) => void, throttleTimer: number = 0) {
    if (throttleTimer > 0) {
      cb = throttle(cb, throttleTimer);
    }
    this.cbs[event] = [...(this.cbs[event] || []), cb];
    return () => {
      this.cbs[event].splice(this.cbs[event].indexOf(cb), 1);
    };
  }
}
